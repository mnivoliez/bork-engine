#pragma once

#include "Bork/Core.hpp"

#include "Bork/Application.hpp"
#include "Bork/Layer.hpp"
#include "Bork/Log.hpp"

// --Entry Point--
#include "Bork/EntryPoint.hpp"
