#pragma once

#ifdef BK_PLATFORM_WINDOWS
#define DEBUG_BREAK() __debugbreak()
#ifdef BK_BUILD_DLL
#define BORK_API __declspec(dllexport)
#else
#define BORK_API __declspec(dllimport)
#endif
#else
#define BORK_API
#define DEBUG_BREAK() __builtin_trap()
#endif

#ifdef BK_DEBUG
#define BK_ENABLE_ASSERTS
#endif

#ifdef BK_ENABLE_ASSERTS
#define BK_ASSERT(x, ...)                                                      \
  {                                                                            \
    if (!(x))                                                                  \
    {                                                                          \
      BK_ERROR("Assertion Failed: {0}", __VA_ARGS__);                          \
      DEBUG_BREAK();                                                           \
    }                                                                          \
  }
#define BK_CORE_ASSERT(x, ...)                                                 \
  {                                                                            \
    if (!(x))                                                                  \
    {                                                                          \
      BK_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__);                     \
      DEBUG_BREAK();                                                           \
    }                                                                          \
  }
#else
#define BK_ASSERT(x, ...)
#define BK_CORE_ASSERT(x, ...)
#endif

#define BIT(x) (1 << x)
