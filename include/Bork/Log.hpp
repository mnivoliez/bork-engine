#pragma once
#include "bkpch.hpp"

#include <spdlog/fmt/ostr.h>
#include <spdlog/spdlog.h>

#include "Core.hpp"

namespace Bork {
  class BORK_API Log
  {
  public:
    static void Init();
    inline static std::shared_ptr<spdlog::logger> &GetCoreLogger()
    {
      return s_CoreLogger;
    }
    inline static std::shared_ptr<spdlog::logger> &GetClientLogger()
    {
      return s_ClientLogger;
    }

  private:
    static std::shared_ptr<spdlog::logger> s_CoreLogger;
    static std::shared_ptr<spdlog::logger> s_ClientLogger;
  };
} // namespace Bork

#define BK_CORE_CRITICAL(...)                                                  \
  ::Bork::Log::GetCoreLogger()->critical(__VA_ARGS__)
#define BK_CORE_ERROR(...) ::Bork::Log::GetCoreLogger()->error(__VA_ARGS__)
#define BK_CORE_WARN(...) ::Bork::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define BK_CORE_INFO(...) ::Bork::Log::GetCoreLogger()->info(__VA_ARGS__)
#define BK_CORE_TRACE(...) ::Bork::Log::GetCoreLogger()->trace(__VA_ARGS__)

#define BK_CRITICAL(...) ::Bork::Log::GetClientLogger()->critical(__VA_ARGS__)
#define BK_ERROR(...) ::Bork::Log::GetClientLogger()->error(__VA_ARGS__)
#define BK_WARN(...) ::Bork::Log::GetClientLogger()->warn(__VA_ARGS__)
#define BK_INFO(...) ::Bork::Log::GetClientLogger()->info(__VA_ARGS__)
#define BK_TRACE(...) ::Bork::Log::GetClientLogger()->trace(__VA_ARGS__)
