#pragma once

#include "Bork/Core.hpp"

#include "Bork/Event/ApplicationEvent.hpp"
#include "Bork/Event/Event.hpp"
#include "Bork/LayerStack.hpp"
#include "Bork/Window.hpp"

namespace Bork {
  class BORK_API Application
  {
  public:
    Application();
    virtual ~Application();

    void Run();

    void OnEvent(Event &);

    void PushLayer(Layer *layer);
    void PushOverlay(Layer *overlay);

  private:
    bool OnWindowClose(WindowCloseEvent &);

    std::unique_ptr<Window> m_Window;
    bool m_Running = true;
    LayerStack m_LayerStack;
  };

  // To be defined in client
  Application *CreateApplication();
} // namespace Bork
