#pragma once

#include "Application.hpp"

extern Bork::Application *Bork::CreateApplication();

int main(int argc, char **argv)
{
  Bork::Log::Init();
  BK_CORE_INFO("Initialize Bork Engine Logs!");
  BK_INFO("Welcome into Bork!");

  auto app = Bork::CreateApplication();
  app->Run();
  delete app;
}
