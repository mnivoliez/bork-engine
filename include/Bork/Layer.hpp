#pragma once

#include "Bork/Core.hpp"
#include "Bork/Event/Event.hpp"

namespace Bork {

  class BORK_API Layer
  {
  public:
    Layer(const std::string &name = "Layer");
    virtual ~Layer();

    virtual void OnAttach() {}
    virtual void OnDetach() {}
    virtual void OnUpdate() {}
    virtual void OnEvent(Event &) {}

    inline const std::string &GetName() const { return m_DebugName; }

  protected:
    std::string m_DebugName;
  };
} // namespace Bork
