#pragma once

#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <utility>

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Bork/Log.hpp"

#ifdef BK_PLATEFORM_WINDOWS
#include <Windows.h>
#endif
