#include "Bork/Application.hpp"

#include "Bork/Event/ApplicationEvent.hpp"

#include <glad/glad.h>

#include <GLFW/glfw3.h>

namespace Bork {

#define BIND_EVENT_FN(x) std::bind(&x, this, std::placeholders::_1)

  Application::Application()
  {
    m_Window = std::unique_ptr<Window>(Window::Create());
    m_Window->SetEventCallback(BIND_EVENT_FN(Application::OnEvent));
  }
  Application::~Application() {}

  void Application::Run()
  {
    while (m_Running)
    {
      glClearColor(1, 0, 1, 1);
      glClear(GL_COLOR_BUFFER_BIT);

      for (Layer *layer : m_LayerStack)
      {
        layer->OnUpdate();
      }

      m_Window->OnUpdate();
    }
  }

  void Application::OnEvent(Event &event)
  {
    EventDispatcher dispatcher(event);
    dispatcher.Dispatch<WindowCloseEvent>(BIND_EVENT_FN(Application::OnWindowClose));
    BK_CORE_TRACE("{0}", event);

    for (auto it = m_LayerStack.end(); it != m_LayerStack.begin();)
    {
      (*--it)->OnEvent(event);
      if (event.Handled)
      {
        break;
      }
    }
  }

  bool Application::OnWindowClose(WindowCloseEvent &event)
  {
    m_Running = false;
    return true;
  }

  void Application::PushLayer(Layer *layer) { m_LayerStack.PushLayer(layer); }

  void Application::PushOverlay(Layer *overlay) { m_LayerStack.PushOverlay(overlay); }

} // namespace Bork
