#include "bkpch.hpp"

#include "Bork/Layer.hpp"

namespace Bork {
  Layer::Layer(const std::string &debugName) : m_DebugName(debugName) {}

  Layer::~Layer() {}
} // namespace Bork
