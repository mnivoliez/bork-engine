#include <BorkEngine.hpp>

class Sandbox : public Bork::Application
{
public:
  Sandbox() {}
  ~Sandbox() {}
};

Bork::Application *Bork::CreateApplication() { return new Sandbox(); }
