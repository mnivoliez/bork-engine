# Bork Engine

Bork Engine, because every game should be borked out.

Engine heavily inspired by the [Hazel engine](https://github.com/TheCherno/Hazel) 
and the associated [video series](https://www.youtube.com/watch?v=JxIZbV_XjAs&list=PLlrATfBNZ98dC-V-N3m0Go4deliW), as well as my attempt at the [Linux Game Jam 2019](https://gitlab.deep-nope.me/mnivoliez/linuxgamejam-2019)